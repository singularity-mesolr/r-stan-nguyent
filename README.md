# r-stan-nguyent

R-stan container 

## BUILD
```bash
singularity build r-stan-nguyent.sif r-stan-nguyent.def
```

## USAGE
```bash
singularity pull r-stan-nguyent.sif oras://registry.forgemia.inra.fr/singularity-mesolr/r-stan-nguyent/r-stan-nguyent:latest
singularity exec r-stan-nguyent.sif bash -c "/opt/jams/jams.sh"
```
